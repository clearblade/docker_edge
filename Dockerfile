# syntax=docker/dockerfile:1

##
## Creates a docker image that contains the ClearBlade Edge and a default configuration file. See associated
## README.md file for additional information.
##
## The Edge binary will reside in the /usr/local/bin directory
## The Edge database will be named edge.db and will reside in the /var/lib/clearblade directory
## The Edge configuration file will be named config.toml and reside in the /etc/clearblade directory
##

FROM alpine:latest
LABEL description="Runtime container - ClearBlade Edge"

ENV CONFIG_PATH=/etc/clearblade
ENV CB_VAR_PATH=/var/lib/clearblade
ENV EDGE_DB_DIR=$CB_VAR_PATH/db
ENV EDGE_DB_PATH=$EDGE_DB_DIR/edge.db
ENV ADAPTERS_ROOT_DIR=$CB_VAR_PATH
ENV CONFIG_FILE=$CONFIG_PATH/config.toml

ARG EdgeVersion=9.13.0
ARG PlatformIp=platform.clearblade.com
ARG PlatformPort=8951
ARG ParentSystemKey
ARG EdgeIp=localhost
ARG EdgePrivateIP=localhost
ARG EdgeId
ARG EdgeCookie

ARG ProvisioningMode=false
ARG ProvisioningSystem
ARG ProvisionalSqliteAdmin=./prov_clearblade.db
ARG ProvisionalSqliteUserdata=./prov_clearblade_users.db

ARG HttpPort=:9000
ARG GzipResponses=true

ARG BrokerTCPPort=1883
ARG BrokerTLSPort=:1884
ARG BrokerWSPort=:8903
ARG BrokerWSSPort=:8904
ARG MessagingAuthPort=:8905
ARG MessagingAuthWSPort=:8907
ARG MaxPublishSize=5MB
ARG AllowDuplicateClientId=false

ARG ExpireTokens=true
ARG InsecureAuth=false
ARG Insecure=false
ARG ExpireDevTokens=true

ARG Logfile=stdout
ARG MaxLogFileSizeInKB=-1
ARG MaxLogFileBackups=1
ARG LogLevel=info
ARG LoggingEnabled=true
ARG Includes
ARG Excludes

ARG DBHost=127.0.0.1
ARG DBPort=5432
ARG DBUsername=cbuser
ARG DBPassword=ClearBlade2014

ARG LeanMode=false 
ARG StoreAnalytics=true
ARG StoreMessageHistory=true
ARG StoreCodeLogs=true
ARG MaxCodeLogs=25
ARG MaxAuditTrailDays=7

ARG RPCTransport="tcp"
ARG RPCPort="8950"
ARG RPCTimeout=120
ARG RPCKeepaliveInterval=60

ARG E2PInsertOnDeploy=true
ARG SyncOptimize=false
ARG SyncOptimizeExceptions=""
ARG SyncOptimizations=""
ARG SyncDistributeWaitTime=1

ARG DeletionEnabled=true
ARG ExpirationAgeSeconds=1209600
ARG MaxRowCount=10000
ARG MaxSizeKb=15000
ARG TimePeriodDeleteMsgHistory=120

#Download the specific version of the edge that has been requested
ADD "https://github.com/ClearBlade/edge/releases/download/$EdgeVersion/edge-linux-amd64.tar.gz" .

#Create the directories we will need, untar the edge binary, make sure the binary is executable, and clean up after ourselves
RUN mkdir -p $CONFIG_PATH; \
	mkdir -p $CB_VAR_PATH; \
	mkdir -p $EDGE_DB_DIR; \
  tar xzvf edge-linux-amd64.tar.gz; \
  mv ./edge /usr/local/bin/edge;\
  chmod +x /usr/local/bin/edge; \
  rm edge-linux-amd64.tar.gz;

#Create the edge configuration file and store in the docker image
RUN echo $'Title = "ClearBlade Edge Configuration File"\n\
\n\
[Edge]\n\
EdgeID = "'$EdgeId$'" # (string) Edge name\n\
PlatformIP = "'$PlatformIp$'" # (string) The IP address of the platform without port\n\
PlatformPort = "'$PlatformPort$'" # (string) RPC port of the platform. Defaults to 8951 for TLS\n\
EdgeCookie = "'$EdgeCookie$'" # (string) The cookie for the edge session\n\
ParentSystemKey = "'$ParentSystemKey$'" # (string) The parent system of the edge\n\
EdgeIP = "'$EdgeIp$'" # (string) The edge IP. Defaults to localhost\n\
EdgePrivateIP = "'$EdgePrivateIP$'" # (string) The edge IP. Defaults to localhost\n\
\n\
[Adaptors]\n\
AdaptorsRootDir = "'$ADAPTERS_ROOT_DIR$'" # (string) Directory where adaptor files are stored. Defaults to /var/lib/clearblade\n\
\n\
[Provisioning]\n\
ProvisioningMode = '$ProvisioningMode$' # (boolean) Need to provison (point at a platform) before edge is functional (default: false)\n\
ProvisioningSystem = "'$ProvisioningSystem$'" # (string) System key of the provisioning system (default: "")\n\
ProvisionalSqliteAdmin = "'$ProvisionalSqliteAdmin$'" # (string) Location of the provisioning sqlite admin db file\n\
\n\
[HTTP]\n\
HttpPort = "'$HttpPort$'" # (string) Listen port for the HTTP server\n\
GzipResponses = '$GzipResponses$' # (boolean) gzip http responses if http client supports it\n\
\n\
[MQTT]\n\
BrokerTCPPort = "'$BrokerTCPPort$'" # (string) Listen port for MQTT broker\n\
BrokerTLSPort = "'$BrokerTLSPort$'" # (string) TLS listen port for MQTT broker\n\
BrokerWSPort = "'$BrokerWSPort$'" # (string) Websocket listen port for MQTT broker\n\
BrokerWSSPort = "'$BrokerWSSPort$'" # (string) TLS websocket listen port for MQTT broker\n\
MessagingAuthPort = "'$MessagingAuthPort$'" # (string) Listen port for MQTT Auth broker\n\
MessagingAuthWSPort = "'$MessagingAuthWSPort$'" # (string) Websocket listen port for MQTT Auth broker\n\
MaxPublishSize = "'$MaxPublishSize$'" # set the maximum allowed publish size in bytes. accepts INT{mb,kb}. If no modifier is present, will assume bytes\n\
AllowDuplicateClientId = '$AllowDuplicateClientId$' # Allow a new connection with a duplicate client id; kill the existing connection\n\
\n\
[Security]\n\
ExpireTokens = '$ExpireTokens$' # (boolean) Set to invalidate user/device tokens issued more than the system tokenTTL (defaults to 5 days). Dev tokens will not be removed\n\
InsecureAuth = '$InsecureAuth$' # (boolean) Disables password hashing if set to true. Used only for development\n\
Insecure = '$Insecure$' # (boolean) Disables edge to platform TLS communication if set to true. Used only for development\n\
ExpireDevTokens = '$ExpireDevTokens$' # (boolean) Set to invalidate developer tokens issued more than the instance tokenTTL (defaults to 5 days)\n\
\n\
[Logging]\n\
Logfile = "'$Logfile$'" # (string) Location of logfile. If the value "stderr" or "stdout" are supplied, then it will forward to their respective file handles\n\
MaxLogFileSizeInKB = '$MaxLogFileSizeInKB$' # (int64) Maximum size of log file before rotation in KB. Must be greater than 100 KB. -1 indicates no limit\n\
MaxLogFileBackups = '$MaxLogFileBackups$' # (int) Maximum backups of the log file. Must be greater than 1\n\
LogLevel = "'$LogLevel$'" # (string) Raise minimum log-level (debug,info,warn,error,fatal)\n\
LoggingEnabled = '$LoggingEnabled$' # enable or disable logging\n\
Includes = "'$Includes$'" # logging categories to be allowed when logging\n\
Excludes = "'$Excludes$'" # logging categories to be not allowed when logging\n\
\n\
[Database]\n\
DBStore = "sqlite" # (string) Database store to use. postgres for platform and sqlite for edge\n\
DBHost = "'$DBHost$'" # (string) Address of the database server\n\
DBPort = "'$DBPort$'" # (string) Database port\n\
DBUsername = "'$DBUsername$'" # (string) Username for connecting to the database\n\
DBPassword = "'$DBPassword$'" # (string) Password for connecting to the database\n\
DBType = "sqlite"\n\
SqliteAdmin = "'$EDGE_DB_PATH$'" # (string) Location for storing sqlite database file\n\
Local = false # (boolean) Use only local cache for storage. Used only for development\n\
\n\
[Debug]\n\
DevelopmentMode = false # (boolean) Enables debug messages and triggers. Used only for development\n\
DisablePprof = true # (boolean) This will disable pprof output file creation and pprof web-server creation if set to true\n\
PprofCPUInterval = 10 # (int) The length of time, in minutes, to wait between successive pprof cpu profile generations\n\
PprofHeapInterval = 10 # (int) The length of time, in minutes, to wait between successive pprof heap profile generations\n\
PprofMaxFiles = 30 # (int) The maximum number of cpu and heap profiles to retain. 0 indicates keep all of them\n\
PprofMaxFileAge = 1440 # (int) The maximum amount of time, specified in minutes, in which to retain cpu and heap profile data files\n\
MaxPanicsStored = 1000 # How many panics can be stored in the db (default: 1000)\n\
EnablePrometheus = false # Collect debug metrics and expose prometheus exporter endpoints (default: false)\n\
DumpDuktapeContext = false # Dump Duktape Context (default: false)\n\
\n\
[LeanMode]\n\
LeanMode = '$LeanMode$' # (boolean) Stop storing analytics, message history and code logs if set to true\n\
StoreAnalytics = '$StoreAnalytics$' # (boolean) Stop storing analytics if set to false\n\
StoreMessageHistory = '$StoreMessageHistory$' # (boolean) Stop storing message history if set to false\n\
StoreCodeLogs = '$StoreCodeLogs$' # (boolean) Stop storing code logs if set to false\n\
MaxCodeLogs = '$MaxCodeLogs$' # (int) Maximum number of most recent code logs to keep (Default: 25)\n\
MaxAuditTrailDays = '$MaxAuditTrailDays$' # (int) Number of days of audit trail to keep\n\
MaxAuditTrailCounts = 500 # Max number of audit trail entries per (asset_class,action_type) (default: 500)\n\
DisableDebugGoroutines = false # Disable all goroutines that print debug info\n\
MaxNotificationHistory = 500 # Max number of noticiation history entries (default: 500)\n\
\n\
[RPC]\n\
RPCTransport = "'$RPCTransport$'" # (string) Transport layer for RPC communications\n\
RPCPort = "'$RPCPort$'" # (string) Listen port for external RPC server. Used to edge to platform communication\n\
RPCTimeout = '$RPCTimeout$' # (int) Timeout for all RPC calls either within the platform or from platform to edge\n\
RPCKeepaliveInterval = '$RPCKeepaliveInterval$' # (int) Keepalive interval for RPC connections\n\
MaxRPCPacketSizeMB = 20 # Maximum RPC packet size after which packets will be split into segments and transferred\n\
RPCSegmentSizeMB = 5 # Size of each RPC packet segment\n\
MaxInMemorySegmentStorageSizeMB = 100 # Maximum in memory segment storage size during data transfer after which segments are stored in the database\n\
MaxSegmentStorageDays = 5 # Maximum number of days to store segments in the database\n\
EncoderDecoderType = "json" # encoder decoder type for rpc communication\n\
DataCompression = "lz4" # data compression for rpc communication\n\
\n\
[Sync]\n\
SyncTransport = "mqtt" # (string) (Deprecated)\n\
E2PInsertOnDeploy = '$E2PInsertOnDeploy$' # (boolean) Disables or enables e2p insert behaviour when only deploy is set in a deployment\n\
SyncOptimize = '$SyncOptimize$' # (boolean) Optimizes the syncing process if set to true\n\
SyncOptimizeExceptions = "'$SyncOptimizeExceptions$'" # (string) Exceptions for the sync optimization process\n\
SyncOptimizations = "'$SyncOptimizations$'" # (string) List specific optimizations to run. Default is all\n\
SyncDistributeWaitTime = '$SyncDistributeWaitTime$' # (int) How long to wait before distributing asset sync events\n\
PerSystemOptimizerIntervalSeconds = 3600 # Interval to run the per system optimizer (Currently only for collection item in inserts)\n\
\n\
[MessageHistory]\n\
DeletionEnabled = '$DeletionEnabled$' # (boolean) Sets the default setting for message history autodeletion for new systems\n\
ExpirationAgeSeconds = '$ExpirationAgeSeconds$' # (int64) Sets the default setting for new systems for age at which a message should be deleted\n\
MaxRowCount = '$MaxRowCount$' # (int) Sets the default setting for new systems for maximum rows after message history deletion\n\
MaxSizeKb = '$MaxSizeKb$' # (int) Sets the default setting for message history maximum size for new systems\n\
TimePeriodDeleteMsgHistory = '$TimePeriodDeleteMsgHistory$' # (int) Set the time interval to periodically erase msgHistory and analytics{Time.seconds}\n\
\n\
[Locks]\n\
MaxLockTime = 60 # Number of seconds until locks are force-unlocked (default 60)\n\
MaxLockWaitTime = 120 # Number of seconds to wait for a lock before giving up w/error\n\
CheckLockLock = false # Should we check for and prevent a lock(); lock(); sequence\n\
\n\
[Buckets]\n\
QueueEmptyIntervalSecs = 5 # # of secs to wait before emptying queue full of file storage system events (default 5)\n\
MaxFilePacketSize = '$ExpirationAgeSeconds$' # (int64) Sets the default setting for new systems for age at which a message should be deleted\n\
RootDir = '$MaxRowCount$' # (int) Sets the default setting for new systems for maximum rows after message history deletion\n\
StorageType = '$MaxSizeKb$' # (int) Sets the default setting for message history maximum size for new systems\n\
GoogleBucketName = "clearblade_platform_bucket" # The google cloud storage bucket name\n\
DefaultEdgeStorageType = "local" # Use the local file system\n\
DefaultPlatformStorageType = "local" # Use google buckets\n\
MaxParallelSyncEvents = 10 # Max number of goroutines processing a buckets sync queue (default 10)\n\
\n\
[Triggers]\n\
MaxExecutors = 1024 # Maximum number of executors that can run at once (default 1024)\n\
'\
>> $CONFIG_FILE

#Provide Default arguments to the entrypoint
CMD ["-config=$CONFIG_FILE"]
ENTRYPOINT ["/usr/local/bin/edge"]
