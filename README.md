# Docker Image Creation

## Prerequisites

- Building the image requires internet access

### Creating the Docker image for ClearBlade Edge

Execute the following command to create a docker image for the ClearBlade Edge:  

```
docker build --no-cache -f \
--build-arg EdgeVersion={EDGE_VERSION_NUMBER} \
--build-arg PlatformIp={PLATFORM_IP} \
--build-arg ParentSystemKey={PARENT_SYSTEM_KEY} \
-t clearblade_edge .
```

__Example:__
```
docker build --no-cache \
--build-arg EdgeVersion=9.11.0 \
--build-arg PlatformIp=community.clearblade.com \
--build-arg ParentSystemKey=8ee8d7eb0b84b69bdd899984b84e \
-t clearblade_edge .
```

#### Build Arguments
During the build process, the Edge configuration file (config.toml) will be created and stored in the /etc/clearblade directory in the docker image. The Dockerfile accepts build arguments that can alter the values included in the config.toml file. If the build arguments are not specified in the __docker build__ command, default values will be utilized if applicable. The values in the config.toml file will be able to be overridden when the container is started.

The build arguments that can be specified are as follows:

| Argument | Default | Description |
| --- | --- | --- |
| EdgeVersion | 9.13 | The version of the Edge binary to download from the public repository |
|  |  |  |
| PlatformIp | "platform.clearblade.com" | The IP address of the platform without port |
| PlatformPort | 8951 | The RPC port of the platform. Defaults to 8951 for TLS |
| ParentSystemKey |  | The system key of the parent system of the edge |
|  |  |  |
| EdgeIp | localhost | The edge IP address|
| EdgePrivateIP | localhost | The private IP address of the edge |
| EdgeId |  | The Edge name |
| EdgeCookie |  | The cookie the edge should use to authenticate with the platform |
|  |  |  |
| ProvisioningMode | false | Need to provison (point at a platform) before edge is functional? |
| ProvisioningSystem | | System key of the provisioning system |
| ProvisionalSqliteAdmin | ./prov_clearblade.db | Location of the provisioning sqlite admin db file |
|  |  |  |
| HttpPort | :9000 | Listen port for the HTTP server on Edge |
| GzipResponses | true | gzip http responses if http client supports it |
|  |  |  |
| BrokerTCPPort | :1883 | Listen port for MQTT broker |
| BrokerTLSPort | :1884 | TLS listen port for MQTT broker |
| BrokerWSPort | :8903 | Websocket listen port for MQTT broker |
| BrokerWSSPort | :8904 | TLS websocket listen port for MQTT broker |
| MessagingAuthPort | :8905 | Listen port for MQTT Auth broker |
| MessagingAuthWSPort | :8907 | Websocket listen port for MQTT Auth broker |
| MaxPublishSize | 5MB | set the maximum allowed publish size in bytes. accepts INT{mb,kb}. If no modifier is present, will assume bytes |
| AllowDuplicateClientId | false | Allow a new connection with a duplicate client id; kill the existing connection |
|  |  |  |
| ExpireTokens | true | Set to invalidate user/device tokens issued more than the system tokenTTL (defaults to 5 days). Dev tokens will not be removed |
| InsecureAuth | false | Disables password hashing if set to true. Used only for development |
| Insecure | false | Disables edge to platform TLS communication if set to true. Used only for development |
| ExpireDevTokens | true | Set to invalidate developer tokens issued more than the instance tokenTTL (defaults to 5 days) |
|  |  |  |
| Logfile | stdout | Location of logfile. If the value "stderr" or "stdout" are supplied, then it will forward to their respective file handles |
| MaxLogFileSizeInKB | -1 | Maximum size of log file before rotation in KB. Must be greater than 100 KB. -1 indicates no limit |
| MaxLogFileBackups | 1 | Maximum backups of the log file. Must be greater than 1 |
| LogLevel | info | Raise minimum log-level (debug,info,warn,error,fatal) |
| LoggingEnabled | true | Enable or disable logging |
| Includes |  | logging categories to be allowed when logging |
| Excludes |  | logging categories to be not allowed when logging |
|  |  |  |
| DBHost | 127.0.0.1 | Address of the database server |
| DBPort | 5432 | Database port |
| DBUsername | cbuser | Username for connecting to the database |
| DBPassword | ClearBlade2014 | Password for connecting to the database |
|  |  |  |
| LeanMode | false | Stop storing analytics, message history and code logs if set to true |
| StoreAnalytics | true | Stop storing analytics if set to false |
| StoreMessageHistory | true | Stop storing message history if set to false |
| StoreCodeLogs | true | Stop storing code logs if set to false |
| MaxCodeLogs | 25 | Maximum number of most recent code logs to keep |
| MaxAuditTrailDays | 7 | Number of days of audit trail to keep |
|  |  |  |
| RPCTransport | tcp | Transport layer for RPC communications |
| RPCPort | 8950 | Listen port for external RPC server. Used in edge to platform communication |
| RPCTimeout | 60 | Timeout for all RPC calls either within the platform or from platform to edge |
| RPCKeepaliveInterval | 60 | Keepalive interval for RPC connections |
|  |  |  |
| E2PInsertOnDeploy | true | Disables or enables e2p insert behaviour when only deploy is set in a deployment |
| SyncOptimize | false | Optimizes the syncing process if set to true |
| SyncOptimizeExceptions |  | Exceptions for the sync optimization process |
| SyncOptimizations |  | List specific optimizations to run. Default is all |
| SyncDistributeWaitTime | 1 | How long to wait before distributing asset sync events |
|  |  |  |
| DeletionEnabled | true | Sets the default setting for message history autodeletion for new systems |
| ExpirationAgeSeconds | 1209600 | Sets the default setting for new systems for age at which a message should be deleted (seconds)|
| MaxRowCount | 10000 | Sets the default setting for new systems for maximum rows after message history deletion |
| MaxSizeKb | 15000 | Sets the default setting for message history maximum size for new systems |
| TimePeriodDeleteMsgHistory | 120 | Set the time interval to periodically erase message history istory and analytics (seconds) |

#### Clean docker build cache
It is a good idea to clear the Docker Builder cache occasionally. If the results of a build do not contain the latest changes in the Dockerfile, try to run this command.

- ```docker builder prune```

# Using the docker image

## Deploying image

When the docker image has been created, it will need to be saved and imported into the runtime environment. Execute the following steps to save and deploy the adapter image

- On the machine where the ```docker build``` command was executed, execute ```docker save clearblade_edge:latest -o clearblade_edge.tar``` 

- On the server where docker is running, execute ```docker load -i clearblade_edge.tar```

## Executing the image

Once you create the docker image, start the clearblade_edge using the following command as an example:


```docker run -d --name clearblade_edge --restart always clearblade_edge -parent-system=<SYSTEM_KEY> -platform-ip=<PLATFORM_URL> -edge-ip=<EDGE_IP_ADDRESS> -edge-id=<EDGE_NAME> -edge-cookie=<EDGE_TOKEN>```     


```
-parent-system The System Key of your System on the ClearBlade Platform
-platform-ip The address of the ClearBlade Platform (ex. https://platform.clearblade.com:443)
-edge-ip The IP address of the ClearBlade Edge (ex. localhost)
-edge-id The name of the ClearBlade Edge defined in the ClearBlade Platform
-edge-cookie The ClearBlade Edge security token for the Edge defined in the ClearBlade Platform
```

Ex.
```docker run -d --name clearblade_edge --restart always clearblade_edge -platform-ip='community.clearblade.com' -parent-system='8ee8d7eb0b84b69bdd899984b84e' -edge-ip='localhost' -edge-id='Docker_Test' -edge-cookie='225H24T62398Nx4s1222OgV15'```

### Edge Flags
The examples above include a limited number of flags in the __docker run__ command. While the docker image will contain a default ClearBlade Edge configuration file, any configuration option in the configuration file can be overridden on the command line when creating and starting a ClearBlade Edge docker container. The entire list of ClearBlade Edge command line arguments applicable to docker and available to be overridden is as follows:

| Argument Name | Data Type | Description |
| --- | --- | --- |
| platform-ip | String | The IP address of the platform without port |
| platform-port | String | The RPC port of the platform. Defaults to 8951 for TLS |
| parent-system | String | The system key of the parent system of the edge |
|  |  |  |
| edge-ip | String | The edge IP address|
| edge-private-ip | String | The private IP address of the edge |
| edge-id | String | The Edge name |
| edge-cookie | String | The cookie the edge should use to authenticate with the platform |
|  |  |  |
| edge-listen-port | String | Listen port for the HTTP server on Edge, must include colon. Ex. :9000 |
| gzip-responses | Boolean | gzip http responses if http client supports it |
|  |  |  |
| broker-tcp-port | String | Listen port for MQTT broker (do not include the colon) |
| broker-tls-port | String | TLS listen port for MQTT broker, must include colon. Ex. :1884 |
| broker-ws-port | String | Websocket listen port for MQTT broker, must include colon. Ex. :8903 |
| broker-wss-port | String | TLS websocket listen port for MQTT broker, must include colon. Ex. :8904 |
| mqtt-auth-port | String | Listen port for MQTT Auth broker, must include colon. Ex. :8905 |
| mqtt-ws-auth-port | String | Websocket listen port for MQTT Auth broker, must include colon. Ex. :8907 |
| maximum-publish-size | String | set the maximum allowed publish size in bytes. accepts INT{mb,kb}. If no modifier is present, will assume bytes. Ex. 5MB |
| allow-duplicate-client-id | false | Allow a new connection with a duplicate client id; kill the existing connection |
|  |  |  | 
| expire-tokens | Boolean | Set to invalidate user/device tokens issued more than the system tokenTTL (defaults to 5 days). Dev tokens will not be removed |
| insecure-auth | Boolean | Disables password hashing if set to true. Used only for development |
| insecure | Boolean | Disables edge to platform TLS communication if set to true. Used only for development |
| expire-dev-tokens | Boolean | Set to invalidate developer tokens issued more than the instance tokenTTL (defaults to 5 days) |
|  |  |  |
| logfile | String | Location of logfile. If the value "stderr" or "stdout" are supplied, then it will forward to their respective file handles |
| max-logfile-size-kb | Integer | Maximum size of log file before rotation in KB. Must be greater than 100 KB. -1 indicates no limit |
| max-logfile-backups | Integer | Maximum backups of the log file. Must be greater than 1 |
| log-level | String | Raise minimum log-level (debug,info,warn,error,fatal) |
| logging-enabled | Boolean | Enable or disable logging |
| log-includes | String | logging categories to be allowed when logging |
| log-excludes | String | logging categories to be not allowed when logging |
|  |  |  |
| db-host | String | Address of the database server |
| db-port | String | Database port (do not include the colon) |
| db-user | String | Username for connecting to the database |
| db-pass | String | Password for connecting to the database |
|  |  |  |
| max-panics-stored | Integer | How many panics can be stored in the db (default: 1000) |
| enable-prometheus | Boolean | Collect debug metrics and expose prometheus exporter endpoints (default: false) |
| dump-duktape-context | Boolean | Dump Duktape Context (default: false) |
|  |  |  |
| lean-mode | Boolean | Stop storing analytics, message history and code logs if set to true |
| store-analytics | Boolean | Stop storing analytics if set to false |
| store-message-history | Boolean | Stop storing message history if set to false |
| store-logs | Boolean | Stop storing code logs if set to false |
| max-code-logs | Integer | Maximum number of most recent code logs to keep |
| max-audit-trail-days | Integer | Number of days of audit trail to keep |
| max-audit-trail-counts | Integer | Number of days of audit trail to keep |
| disable-debug-goroutines | Boolean | Number of days of audit trail to keep |
| max-notification-history | Integer | Max number of notification history entries (default: 500) |
|  |  |  |
| rpc-transport | String | Transport layer for RPC communications |
| rpc-port | String | Listen port for external RPC server. Used in edge to platform communication |
| rpc-timeout | Integer | Timeout for all RPC calls either within the platform or from platform to edge |
| rpc-keepalive | Integer | Keepalive interval for RPC connections |
| max-rpc-packet-size-mb | Integer | Maximum RPC packet size after which packets will be split into segments and transferred |
| rpc-segment-size-mb | Integer | Size of each RPC packet segment |
| max-in-memory-segment-storage-size-mb | Integer | Maximum in memory segment storage size during data transfer after which segments are stored in the database |
| max-segment-storage-days | Integer | Maximum number of days to store segments in the database |
| encoder-decoder-type | String | ncoder decoder type for rpc communication |
| rpc-data-compression | String | data compression for rpc communication |
|  |  |  |
| adaptors-root-dir | String | directory where adaptor files are stored (default: ./) |
|  |  |  |
| provisioning-mode | Boolean | Need to provison (point at a platform) before edge is functional? |
| provisioning-system | String| System key of the provisioning system |
| provisional-sqlite-path | String | Location of the provisioning sqlite admin db file |
|  |  |  |
| e2p-insert-on-deploy | Boolean | Disables or enables e2p insert behaviour when only deploy is set in a deployment |
| sync-transport | String | Set the type of transport between edge and platform when syncing. Default is mqtt. Other options are amqp, tcp, and stub |
| sync-optimize | Boolean | Optimizes the syncing process if set to true |
| sync-optimize-exceptions | String | Exceptions for the sync optimization process |
| sync-optimizations | String | List specific optimizations to run. Default is all |
| sync-distribute-wait-time | Integer | How long to wait before distributing asset sync events |
| per-system-optimizer-interval | Integer | Interval to run the per system optimizer (Currently only for collection item in inserts) |
|  |  |  |
| msghistory-deletion-default-enabled | Boolean | Sets the default setting for message history autodeletion for new systems |
| msghistory-deletion-default-age-s | Integer | Sets the default setting for new systems for age at which a message should be deleted (seconds) |
| msghistory-deletion-default-max-rows | Integer | Sets the default setting for new systems for maximum rows after message history deletion |
| msghistory-deletion-default-max-size-kb | Integer | Sets the default setting for message history maximum size for new systems |
| period-checkdelete-msghistory | Integer | Set the time interval to periodically erase message history istory and analytics (seconds) |
|  |  |  |
| max-lock-time | Integer | Number of seconds until locks are force-unlocked (default 60) |
| max-lock-wait-time | Integer | Number of seconds to wait for a lock before giving up w/error |
| check-lock-lock | Boolean | Should we check for and prevent a lock(); lock(); sequence |
|  |  |  |
| queue-empty-interval-secs | Integer | # of secs to wait before emptying queue full of file storage system events (default 5) |
| max-file-packet-size | Integer | Maximum number of bytes that can be copied/transferred in a single RPC packet (default 2048) |
| bucket-root-dir | String | Root path to buckets |
| bucket-storage-type | String | The storage mechanism to use |
| google-bucket-name | String | The google cloud storage bucket name |
| default-edge-storage-type | String | Use the local file system |
| max-parallel-bucket-sync-events | Integer | Max number of goroutines processing a bucket's sync queue (default 10) |
|  |  |  |
| max-executors | Integer | Maximum number of executors that can run at once (default 1024) |
